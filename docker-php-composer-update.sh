#!/bin/bash

rm -rf ./tmp-composer-folder
mkdir -p tmp-composer-folder

cp ./site/composer.json ./tmp-composer-folder/composer.json
cp ./site/composer.lock ./tmp-composer-folder/composer.lock

cd tmp-composer-folder
composer update


cp ./composer.json ../site/composer.json
cp ./composer.lock ../site/composer.lock

cd ..

rm -rf ./tmp-composer-folder
