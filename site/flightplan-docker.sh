#!/bin/bash
docker-compose up -d
rm -rf ./tmp-docker-wp-folder
mkdir -p tmp-docker-wp-folder

#Get files installed by composer and other scripts
docker cp $(docker-compose ps -q wordpress):/srv/web ./tmp-docker-wp-folder/web
find ./tmp-docker-wp-folder -type l -delete


# Get files we have created and track in this repo
docker cp $(docker-compose ps -q wordpress):/srv/wp-default-app/plugins/. ./tmp-docker-wp-folder/web/app/plugins
docker cp $(docker-compose ps -q wordpress):/srv/wp-default-app/themes/. ./tmp-docker-wp-folder/web/app/themes
docker cp $(docker-compose ps -q wordpress):/srv/wp-default-app/languages/. ./tmp-docker-wp-folder/web/app/languages
docker cp $(docker-compose ps -q wordpress):/srv/wp-default-app/mu-plugins/. ./tmp-docker-wp-folder/web/app/mu-plugins

docker cp $(docker-compose ps -q wordpress):/srv/wp-composer-app/plugins/. ./tmp-docker-wp-folder/web/app/plugins

docker cp $(docker-compose ps -q wordpress):/srv/vendor ./tmp-docker-wp-folder/vendor
docker cp $(docker-compose ps -q wordpress):/srv/config ./tmp-docker-wp-folder/config
docker cp $(docker-compose ps -q wordpress):/srv/composer.json ./tmp-docker-wp-folder/composer.json
docker cp $(docker-compose ps -q wordpress):/srv/composer.lock ./tmp-docker-wp-folder/composer.lock

docker-compose down