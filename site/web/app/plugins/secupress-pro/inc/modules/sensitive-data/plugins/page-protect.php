<?php
/**
 * Module Name: Page Protect
 * Description: Ask the user's password to enter in their some pages (need hooks to work)
 * Main Module: sensitive_data
 * Author: SecuPress
 * Version: 1.0
 */

defined( 'SECUPRESS_VERSION' ) or die( 'Cheatin&#8217; uh?' );

/**
 * Display a form to unlock a screen.
 *
 * @since 1.0
 * @author Julio Potier
 */
function secupress_shortcut_settings() {
	global $current_user;

	if ( ! is_admin() || ! is_user_logged_in() || get_site_transient( 'secupress_check_password_' . get_current_user_id() ) !== false ) {
		return;
	}

	if ( isset( $_GET['error'] ) ) {
		$_SERVER['REQUEST_URI'] = remove_query_arg( array( 'error' ), $_SERVER['REQUEST_URI'] );
	}

	require_once( ABSPATH . 'wp-admin/admin-header.php' );
	?>
	<h1><?php echo SECUPRESS_PLUGIN_NAME; ?></h1>

	<div style="-webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, .13); box-shadow: 0 1px 3px rgba(0, 0, 0, .13); background: #FFF; padding: 5px 20px; width: 320px;">

		<form id="your-profile" action="<?php echo admin_url( 'admin-post.php' ); ?>" method="post" novalidate="novalidate">

			<h3><?php _e( 'Page Access Security', 'secupress-pro' ); ?></h3>
			<p class="description">
				<?php printf( __( 'For security reasons, you need to enter the password of this account (%s) again to access this page.<br/>Once done, you will have <b>only 5 minutes</b>, then you will have to enter it again.', 'secupress-pro' ), esc_html( $current_user->user_login ) ); ?>
			</p>
			<p>
				<label for="user_pass" style="display:block"><b><?php _e( 'Password' ); ?></b><br />
				<input type="password" name="pwd" id="user_pass" class="input text" style="width:100%" value="" size="20" /></label>
			</p>
			<input type="hidden" name="action" value="secupress_check_password" />
			<input type="hidden" name="user_id" id="user_id" value="<?php echo get_current_user_id(); ?>" />
			<?php wp_nonce_field( 'update-user_' . get_current_user_id() ); ?>

			<p style="text-align:right">
				<?php submit_button( __( 'Continue' ),'primary', 'submit', 0 ); ?>
			</p>

		</form>

	</div>

	<script type="text/javascript">
		try {
			document.getElementById( 'user_pass' ).focus();
		} catch( e ) {};
		if ( typeof wpOnload === 'function' ) {
			wpOnload();
		}
	</script>
	<?php

	include( ABSPATH . 'wp-admin/admin-footer.php' );
	die();
}


add_action( 'admin_post_secupress_check_password', 'secupress_protect_check_password' );
/**
 * Check the password sent to unlock a screen.
 *
 * @since 1.0
 * @author Julio Potier
 */
function secupress_protect_check_password() {
	global $current_user;

	if ( get_site_transient( 'secupress_check_password_' . get_current_user_id() ) !== false ) {
		return;
	}

	if ( isset( $_POST['pwd'], $_POST['_wpnonce'], $_POST['user_id'] ) && get_current_user_id() === (int) $_POST['user_id'] && // WPCS: CSRF ok.
		wp_verify_nonce( $_POST['_wpnonce'], 'update-user_' . get_current_user_id() ) &&
		! is_wp_error( wp_authenticate_username_password( null, $current_user->user_login, $_POST['pwd'] ) )
	) {
		set_site_transient( 'secupress_check_password_' . $_POST['user_id'], '1', 5 * MINUTE_IN_SECONDS );
		wp_safe_redirect( wp_get_referer() );
		die();
	}

	wp_nonce_ays( '' );
}


add_action( 'set_auth_cookie', 'secupress_set_protect_password', 10, 4 );
/**
 * Set the password used on page/profile protection when the user has just logged in.
 *
 * @since 1.0
 * @author Julio Potier
 *
 * @param (string) $auth_cookie Authentication cookie.
 * @param (int)    $expire      The time the login grace period expires as a UNIX timestamp. Default is 12 hours past the cookie's expiration time.
 * @param (int)    $expiration  The time when the authentication cookie expires as a UNIX timestamp. Default is 14 days from now.
 * @param (int)    $user_id     User ID.
 */
function secupress_set_protect_password( $auth_cookie, $expire, $expiration, $user_id ) {
	set_site_transient( 'secupress_check_password_' . $user_id, '1', 5 * MINUTE_IN_SECONDS );
}
