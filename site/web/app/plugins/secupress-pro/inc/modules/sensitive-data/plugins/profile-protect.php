<?php
/**
 * Module Name: Profile Protect
 * Description: Ask the user's password to enter the profile page (needs page-protect).
 * Main Module: sensitive_data
 * Author: SecuPress
 * Version: 1.0
 */

defined( 'SECUPRESS_VERSION' ) or die( 'Cheatin&#8217; uh?' );

add_action( 'admin_init', 'secupress_shortcut_profile_hooks' );
/**
 * Will lock the profile page.
 *
 * @author Julio Potier
 * @since 1.0
 */
function secupress_shortcut_profile_hooks() {
	add_action( 'load-profile.php', 'secupress_shortcut_settings', 0 );
	add_action( 'current_screen', 'secupress_shortcut_maybe_redirect' );
}


/**
 * Deny access to the profile page.
 *
 * @author Julio Potier
 * @since 1.0
 */
function secupress_shortcut_maybe_redirect() {
	global $pagenow;

	if ( 'profile.php' === $pagenow && 'POST' === $_SERVER['REQUEST_METHOD'] && ! empty( $_POST ) && get_site_transient( 'secupress_check_password_' . get_current_user_id() ) === false ) { // WPCS: CSRF ok.
		wp_safe_redirect( add_query_arg( 'error', '1', wp_get_referer() ) );
		die();
	}
}
