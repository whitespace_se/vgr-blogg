<?php
defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

add_action( 'init', 'secupress_init_license_check_cron' );
/**
 * Initiate the cron that will check for the license validity twice-daily.
 *
 * @since 1.0.3
 * @author Grégory Viguier
 */
function secupress_init_license_check_cron() {
	if ( ! wp_next_scheduled( 'secupress_license_check' ) ) {
		wp_schedule_event( time(), 'twicedaily', 'secupress_license_check' );
	}
}


add_action( 'secupress_license_check', 'secupress_license_check_cron' );
/**
 * Cron that will check for the license validity.
 *
 * @since 1.0.3
 * @author Grégory Viguier
 */
function secupress_license_check_cron() {
	if ( ! secupress_is_pro() ) {
		return;
	}

	$url = SECUPRESS_WEB_MAIN . 'key-api/1.0/?' . http_build_query( array(
		'sp_action'  => 'check_pro_license',
		'user_email' => secupress_get_consumer_email(),
		'user_key'   => secupress_get_consumer_key(),
	) );

	$response = wp_remote_get( $url, array( 'timeout' => 10 ) );

	if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {
		return;
	}

	$body = wp_remote_retrieve_body( $response );
	$body = @json_decode( $body );

	if ( ! is_object( $body ) ) {
		return;
	}

	if ( ! empty( $body->success ) && ! empty( $body->data->site_is_pro ) ) {
		// The license is fine.
		return;
	}

	$options = get_site_option( SECUPRESS_SETTINGS_SLUG );
	$options = is_array( $options ) ? $options : array();
	unset( $options['site_is_pro'] );

	if ( ! empty( $body->data->error ) ) {
		// The error code returned by EDD.
		$options['license_error'] = esc_html( $body->data->error );
	}

	secupress_update_options( $options );
}
