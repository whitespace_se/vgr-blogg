<?php


/**
* List recent posts across a Multisite network
*
* @uses get_blog_list(), get_blog_permalink()
*
* @param int $size The number of results to retrieve
* @param int $expires Seconds until the transient cache expires
* @param int $no_cache Force cache busting
* @return object Contains the blog_id, post_id, post_date and post_title
*/
function wp_recent_across_network( $size = 10, $expires = 7200, $no_cache = false ) {
    if( !is_multisite() ) return false;

    // Cache the results with the WordPress Transients API
    // Get any existing copy of our transient data
    if ( $no_cache || ( $recent_across_network = get_site_transient( 'recent_across_network' ) ) === false ) {

        // No transient found, regenerate the data and save a new transient
        // Prepare the SQL query with $wpdb
        global $wpdb;

        $base_prefix = $wpdb->get_blog_prefix(0);
        $base_prefix = str_replace( '1_', '' , $base_prefix );

        // Because the get_blog_list() function is currently flagged as deprecated
        // due to the potential for high consumption of resources, we'll use
        // $wpdb to roll out our own SQL query instead. Because the query can be
        // memory-intensive, we'll store the results using the Transients API
        if ( $no_cache || false === ( $site_list = get_site_transient( 'multisite_site_list' ) ) ) {
            global $wpdb;
            $site_list = $wpdb->get_results( $wpdb->prepare("SELECT * FROM ".$base_prefix."blogs WHERE %d ORDER BY blog_id ASC", 1) );
            set_site_transient( 'multisite_site_list', $site_list, $expires );
        }

        $limit = absint($size);

        // Merge the wp_posts results from all Multisite websites into a single result with MySQL "UNION"
        $query = '';
        foreach ( $site_list as $site ) {
            if( $site == $site_list[0] ) {
                $posts_table = $base_prefix . "posts";
            } else {
                $posts_table = $base_prefix . $site->blog_id . "_posts";
            }

            $posts_table = esc_sql( $posts_table );
            $blogs_table = esc_sql( $base_prefix . 'blogs' );

            //$query .= "(SELECT $posts_table.ID, $posts_table.post_title, $posts_table.post_date, $blogs_table.blog_id FROM $posts_table, $blogs_table\n"; // Faster version but right now we take all fields to support the wp loop
            $query .= "(SELECT $posts_table.*, $blogs_table.blog_id FROM $posts_table, $blogs_table\n";
            $query .= "\tWHERE $posts_table.post_type = 'post'\n";
            $query .= "\tAND $posts_table.post_status = 'publish'\n";
            $query .= "\tAND $blogs_table.blog_id = {$site->blog_id})\n";

            if( $site !== end($site_list) ) {
                $query .= "UNION\n";
            }
            else {
                $query .= "ORDER BY post_date DESC LIMIT 0, %d";
            }
        }

        // Sanitize and run the query
        $query = $wpdb->prepare($query, $limit);
        $recent_across_network = $wpdb->get_results( $query );

        set_site_transient( 'recent_across_network', $recent_across_network, $expires );
    }

    return $recent_across_network;

}
?>