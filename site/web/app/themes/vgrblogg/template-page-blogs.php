<?php
/**
 * Template Name: Våra bloggar
 */

add_filter('body_class', function($classes) {
    $classes[] = 'site-blogs';
    return $classes;
});

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="site-blogs--header">
				<?php the_title( '<h1 class="site-blogs--title">', '</h1>' ); ?>
				<?php twentyseventeen_edit_link( get_the_ID() ); ?>
			</header><!-- .entry-header -->
			<div class="blogs-wrapper">
				<?php
					$subsites = get_sites(array(
						'site__not_in' => array( 1 ),
					));
					foreach( $subsites as $subsite ) {
						$subsite_id = $subsite->blog_id;
						$blog_info = get_blog_details($subsite_id);
						$name = $blog_info->blogname;
						$url = $blog_info->path;
						switch_to_blog( $subsite_id );
						
						$description = wp_trim_words(get_bloginfo( 'description', 'display' ), 30);
						$header_image_url = get_header_image();
						restore_current_blog();
						?>
							<div class="blog">
								<div class="blog--link-section">
									<a href="<?php echo $url ?>">
										<div class="blog--image">
											<img  class="object-fit_cover" src="<?php echo $header_image_url; ?>"/>
										</div>
									</a>	
									<h3 class="blog--title">
										<a href="<?php echo $url ?>"><?php echo $name; ?></a>
									</h3>
								</div>
								<div class="blog--description"><?php echo $description; ?></div>
							</div>
						<?php
					}
				?>
			</div>
		</article><!-- #post-## -->
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php
get_footer();
