<?php


function vgrblogg_acf_multisite_all_posts_field($field) {

    $subsites = get_sites();
    foreach ($subsites as $subsite) {
        switch_to_blog($subsite->blog_id);

        $blog_posts = get_posts(array( 'posts_per_page' => -1));
        $blog_posts_k_v = array();
        foreach ($blog_posts as $blog_post) {
            $blog_posts_k_v[$subsite->blog_id.'_'.$blog_post->ID] = $blog_post->post_title;
        }
        $field['choices'][$subsite->blogname] = $blog_posts_k_v;

        restore_current_blog();
    }


    return $field;
}
add_filter('acf/load_field/key=field_5bc04214d5c06', 'vgrblogg_acf_multisite_all_posts_field');