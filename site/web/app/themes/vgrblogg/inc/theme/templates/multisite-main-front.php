<?php

function vgrblogg_show_custom_frontpage_on_main_site ($post_templates, $context, $post, $post_type) {
    if (!is_main_site()) { // Remove option to set this template on all blogs except the main blog
        unset($post_templates['template-page-main-front.php']);
    }
    return $post_templates;
}

add_filter('theme_page_templates', 'vgrblogg_show_custom_frontpage_on_main_site', 10, 4);