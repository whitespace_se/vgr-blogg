<?php 
function vgrblogg_remove_widgets(){
    unregister_sidebar('sidebar-1');
    if (!is_main_site()) {
        unregister_sidebar('sidebar-2');
        unregister_sidebar('sidebar-3');
    }
}

add_action( 'widgets_init', 'vgrblogg_remove_widgets', 11 );


function vgrblogg_footer_widgets_all_sites() {
    if ( ! is_main_site())
        return;
 
    if (! ( $vgrblogg_footer_widgets = get_site_transient( 'vgrblogg_footer_widgets' ) ) ) {
        // start output buffer
        ob_start();
 
        if ( is_active_sidebar( 'sidebar-2' ) ) {
        ?>
            <div class="widget-column footer-widget-1">
                <?php dynamic_sidebar( 'sidebar-2' ); ?>
            </div>
        <?php
        }
        if ( is_active_sidebar( 'sidebar-3' ) ) {
        ?>
            <div class="widget-column footer-widget-2">
                <?php dynamic_sidebar( 'sidebar-3' ); ?>
            </div>
        <?php }
 
        // grab the data from the output buffer and put it in our variable
        $vgrblogg_footer_widgets = ob_get_contents();
        ob_end_clean();
 
        // Put sidebar into a transient for 4 hours
        set_site_transient( 'vgrblogg_footer_widgets', $vgrblogg_footer_widgets, 4*60*60 );
    }
}
add_action( 'init', 'vgrblogg_footer_widgets_all_sites' );

function vgrblogg_enqueue_ajax_scripts() {
    global $current_screen;
 
    // Only register these scripts if we're on the widgets page
    if ( $current_screen->id == 'widgets' ) {
        wp_enqueue_script( 'vgrblogg_ajax_scripts', get_stylesheet_directory_uri() . '/assets/dist/' . vgrblogg_cache_bust('js/admin-widgets.min.js'), array( 'jquery' ), 1, true );
        wp_localize_script( 'vgrblogg_ajax_scripts', 'vgrblogg_AJAX', array( 'vgrblogg_widget_nonce' => wp_create_nonce( 'widget-update-nonce' ) ) );
    }
}
add_action( 'admin_enqueue_scripts', 'vgrblogg_enqueue_ajax_scripts' );

 
/**
 * AJAX Helper to delete our transient when a widget is saved
 */
function vgrblogg_ajax_reset_transient() {
    // Delete our footer transient.  This runs when a widget is saved or updated.  Only do this if our nonce is passed.
    if ( 
        (! empty( $_REQUEST['vgrblogg-widget-nonce'] ) && wp_verify_nonce( $_REQUEST['vgrblogg-widget-nonce'], 'widget-update-nonce' )) ||
        (! empty($_REQUEST['savewidgets']) && wp_verify_nonce( $_REQUEST['savewidgets'], 'save-sidebar-widgets' ))

    ) {
        delete_site_transient( 'vgrblogg_footer_widgets' );
    }
}
add_action( 'wp_ajax_vgrblogg-reset-transient', 'vgrblogg_ajax_reset_transient', 1 );
add_action( 'wp_ajax_widgets-order', 'vgrblogg_ajax_reset_transient', 1);
