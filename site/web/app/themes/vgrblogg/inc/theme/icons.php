<?php

function vgrblogg_include_svg_icons() {
	// Define SVG sprite file.

	$svg_icons = get_stylesheet_directory(). '/assets/dist/' . vgrblogg_cache_bust('images/vgrblogg-icons.svg');

	// If it exists, include it.
	if ( file_exists( $svg_icons ) ) {
		require_once( $svg_icons );
	}
}
add_action( 'wp_footer', 'vgrblogg_include_svg_icons', 9999 );