<?php

function vgrblogg_enqueue_scripts() {
    wp_enqueue_style('vgrblogg-parent-theme', get_template_directory_uri() .'/style.css');

wp_enqueue_style( 'roboto-font', 'https://fonts.googleapis.com/css?family=Roboto:400,500,700,900|Roboto+Slab:400,500,700,900');
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/assets/dist/' . vgrblogg_cache_bust('css/main.min.css'),
        array(
        	'vgrblogg-parent-theme',
        	'twentyseventeen-fonts',
        	'twentyseventeen-style',
        	'roboto-font'
        ),
        '1.0'
    );
}
add_action('wp_enqueue_scripts', 'vgrblogg_enqueue_scripts');

function vgrblogg_dequeue_scripts() {
    wp_dequeue_script('twentyseventeen-global');
}
add_action('wp_enqueue_scripts', 'vgrblogg_dequeue_scripts', 1000);


    

