<?php

if ( ! function_exists( 'vgrblogg_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function vgrblogg_entry_footer() {

		/* translators: used between list items, there is a space after the comma */
		$separate_meta = __( ', ', 'twentyseventeen' );

		// Get Categories for posts.
		$categories_list = get_the_category_list( $separate_meta );

		// Get Tags for posts.
		$tags_list = get_the_tag_list( '', $separate_meta );

		// We don't want to output .entry-footer if it will be empty, so make sure its not.
		if ( ( ( twentyseventeen_categorized_blog() && $categories_list ) || $tags_list ) || get_edit_post_link() ) {

			echo '<footer class="entry-footer">';

			if ( 'post' === get_post_type() ) {
				if ( ( $categories_list && twentyseventeen_categorized_blog() ) || $tags_list ) {
					echo '<span class="cat-tags-links">';

						// Make sure there's more than one category before displaying.
					if ( $categories_list && twentyseventeen_categorized_blog() ) {
						echo '<span class="cat-links">' . twentyseventeen_get_svg( array( 'icon' => 'folder' ) ) . '<p class="cat-links-title">Kategori: </p>' . '<span class="screen-reader-text">' . __( 'Categories', 'twentyseventeen' ) . '</span>' . $categories_list . '</span>';
					}

					if ( $tags_list && ! is_wp_error( $tags_list ) ) {
						echo '<span class="tags-links">' . twentyseventeen_get_svg( array( 'icon' => 'tag' ) ) . '<p class="tags-links-title">Taggar: </p>' . '<span class="screen-reader-text">' . __( 'Tags', 'twentyseventeen' ) . '</span>' . $tags_list . '</span>';
					}

					echo '</span>';
				}
			}

			twentyseventeen_edit_link();

			echo '</footer> <!-- .entry-footer -->';
		}
	}
endif;
