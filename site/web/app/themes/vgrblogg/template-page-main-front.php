<?php
/**
 * Template Name: Huvudsida
 */

add_filter('body_class', function($classes) {
    $classes[] = 'main-blog-front';
    return $classes;
});

get_header(); ?>
<div class="wrap">
	<div id="" class="post-page">
		<main id="main" class="site-main" role="main">
			<h1 class="front-page-title">Senaste inlägg</h1>
			<?php
			$network_posts = wp_recent_across_network(9);
			if ( count($network_posts) ) :

				/* Start the Loop */
				global $post;
   				$old_post = $post; ?>
   				<div class="blogs-wrapper">
   				<?php
				foreach ( $network_posts as $network_post ) :
					// Assign your post details to $post (& not any other variable name!!!!)
					switch_to_blog( $network_post->blog_id );
					$post = $network_post;
					setup_postdata($post); ?>
					<div class="blog">
						<?php get_template_part( 'template-parts/page/content'); ?>
					</div>
				<?php
					wp_reset_postdata();
					restore_current_blog();
				endforeach;
   				$post = $old_post; ?>
   				</div>
				<?php
			else :

				get_template_part( 'template-parts/post/content', 'none' );

			endif;
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php
get_footer();
