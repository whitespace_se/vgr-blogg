<?php if ( is_multisite() ) : ?>
<div class="navigation-about">
	<div class="navigation-about--container wrap">
		<div class="navigation-about--home">
			<a href="<?php echo network_site_url(); ?>">
				<img class="navigation-about--logo" src="<?php echo get_stylesheet_directory_uri() . '/assets/dist/' . vgrblogg_cache_bust('images/logotype.png'); ?>"/>
			</a>
		</div>
		<div class="navigation-about--blog-title">		
			<h1>
				<?php echo get_bloginfo('name', 'display'); ?>
			</h1>
		</div>
	</div>
</div>
<?php endif; ?>
