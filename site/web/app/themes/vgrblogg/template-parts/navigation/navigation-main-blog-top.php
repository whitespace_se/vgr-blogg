<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<?php if ( is_multisite() ) : ?>
<div class="navigation-top">
	<?php get_template_part( 'template-parts/navigation/navigation-main-blog', 'about' ); ?>
	<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'twentyseventeen' ); ?>">
		<div class="wrap">
			<button class="menu-toggle" aria-controls="top-menu" aria-expanded="false">
				<?php
				echo twentyseventeen_get_svg( array( 'icon' => 'bars' ) );
				echo twentyseventeen_get_svg( array( 'icon' => 'close' ) );
				_e( 'Menu', 'twentyseventeen' );
				?>
			</button>
			<?php
				global $blog_id;
				$switched = false;
				if (!is_main_site()) {
					switch_to_blog( 1 );
					$switched = true;
				}

				wp_nav_menu(
					array(
						'theme_location' => 'top',
						'menu_id'        => 'top-menu',
					)
				);

				if ($switched) {
					restore_current_blog(); 
				}
			?>
		</div>
	</nav><!-- #site-navigation -->
<?php if (!is_main_site() && has_nav_menu( 'top' ) ) : ?>
	<nav id="site-navigation-secondary" class="main-navigation main-navigation--secondary" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'twentyseventeen' ); ?>">
		<div class="wrap">
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'top',
					'menu_id'        => 'top-menu',
				)
			);
			?>
		</div>
	</nav>
<?php endif; ?>
</div>
<?php endif; ?>
