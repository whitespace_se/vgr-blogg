<div class="blog--link-section">
	<a href="<?php echo get_the_permalink(); ?>">
		<div class="blog--image">
		
			<img class="object-fit_cover" src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?: get_stylesheet_directory_uri() . '/assets/dist/' . vgrblogg_cache_bust('images/fallback-vgrblogg.png'); ?>"/>
		
		</div>
	</a>	
	<h3 class="blog--title">
		<a href="<?php echo get_the_permalink(); ?>"><?php echo wp_trim_words(get_the_title(), 6); ?></a>
	</h3>
	<h5 class="blog--title-secondary"><?php echo bloginfo( 'blogname', 'display' ); ?></h5>
	<div class="blog--entry-meta"><?php twentyseventeen_posted_on(); ?></div>
</div>
<div class="blog--description"><?php echo wp_trim_words(get_the_excerpt(), 20); ?></div>
