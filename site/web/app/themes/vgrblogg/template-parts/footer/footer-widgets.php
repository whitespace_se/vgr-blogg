<?php
/**
 * Displays footer widgets if assigned
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<?php
    $show_footer_sidebar = get_site_transient( 'vgrblogg_footer_widgets' );
    if (!empty($show_footer_sidebar)) :
?>

	<aside class="widget-area" role="complementary" aria-label="<?php esc_attr_e( 'Footer', 'twentyseventeen' ); ?>">
		<?php echo $show_footer_sidebar; ?>
	</aside><!-- .widget-area -->
<?php endif; ?>