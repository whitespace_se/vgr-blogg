<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	if ( is_sticky() && is_home() ) :
		echo twentyseventeen_get_svg( array( 'icon' => 'thumb-tack' ) );
	endif;
	?>
	<div class="post-thumbnail post-thumbnail-wrapper">
		<a class="post-thumbnail-wrapper" href="<?php the_permalink(); ?>">
			<div class="post-thumbnail">
				<?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
					<?php the_post_thumbnail( 'twentyseventeen-featured-image' ); ?>
				<?php endif; ?>
			</div><!-- .post-thumbnail -->
		</a>
	</div>
	<div class="post-content">
		<header class="entry-header">
			<?php
			if ( is_single() ) {
				the_title( '<h1 class="entry-title">', '</h1>' );
			} elseif ( is_front_page() && is_home() ) {
				the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}

			if ( 'post' === get_post_type() ) {
				echo '<div class="entry-meta">';
				if ( is_single() ) {
					twentyseventeen_posted_on();
				} else {
					echo twentyseventeen_time_link();
					twentyseventeen_edit_link();
				};
				echo '</div><!-- .entry-meta -->';
			};
			?>
		</header><!-- .entry-header -->

		

		<div class="entry-content">
			<?php
			/* translators: %s: Name of current post */
			if (is_single()) :
			the_content(
				sprintf(
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
					get_the_title()
				)
			);
			else: ?>
				<p><?php echo wp_trim_words(get_the_excerpt(), 30); ?></p>
			<?php
				echo sprintf('<p><a href="%s" class="more-link">%s</a></p>', get_the_permalink(), __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ));
			endif;

			wp_link_pages(
				array(
					'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
					'after'       => '</div>',
					'link_before' => '<span class="page-number">',
					'link_after'  => '</span>',
				)
			);

			if ( is_front_page() && is_home() ) {
				$separate_meta = __( ', ', 'twentyseventeen' );
				$categories_list = get_the_category_list( $separate_meta );
				if ( ( $categories_list && twentyseventeen_categorized_blog() )) {
					echo '<span class="cat-tags-links">';
					echo '<span class="cat-links">' . twentyseventeen_get_svg( array( 'icon' => 'folder' ) ) . '<p class="cat-links-title">Kategori: </p>' . '<span class="screen-reader-text">' . __( 'Categories', 'twentyseventeen' ) . '</span>' . $categories_list . '</span>';
					echo '</span>';
				}
			}
			?>

		</div><!-- .entry-content -->
	</div>
	<?php
	if ( is_single() ) {
		vgrblogg_entry_footer();
	}
	?>

</article><!-- #post-## -->
