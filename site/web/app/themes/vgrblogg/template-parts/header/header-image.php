<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<?php if (function_exists('have_rows') && have_rows('featured_spots') ) : ?>
<?php 
    global $post;
    $old_post = $post; ?>
    <div class="featured-posts">
    <?php 
        $count = 1;
        while(have_rows('featured_spots')): the_row();
        list($blog_id, $post_id) = explode('_', get_sub_field('post'));
        switch_to_blog($blog_id);
        $blog_post = get_post($post_id);
        $post = $blog_post;
        setup_postdata($post); ?>

        <?php
        if ($count === 1) : echo '<div class="featured-posts--column-one">';
        elseif ($count === 2) : echo '<div class="featured-posts--column-two">';
        endif; ?>

        <div class="featured-post-card" style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?>');">
            <a class="featured-post-card--link-container" href="<?php echo get_the_permalink(); ?>">
                <div class="featured-post-card--entry">
                    <h2 class="featured-post-card--title"><?php echo get_the_title(); ?></h2>
                    <span class="featured-post-card--blog-name"><?php echo get_bloginfo('name', 'display'); ?></span>
                </div>
            </a>
        </div>

        <?php
        wp_reset_postdata();
        restore_current_blog(); ?>

        <?php if ($count === 1 || $count === 3) { echo '</div>'; }
        $count++;
        ?>
    <?php endwhile;
    $post = $old_post;?>
    </div>
<?php else : ?>
<div class="custom-header">
		<div class="custom-header-media">
			<?php the_custom_header_markup(); ?>
		</div>
	<?php get_template_part( 'template-parts/header/site', 'branding' ); ?>
</div><!-- .custom-header -->
<?php endif; ?>
