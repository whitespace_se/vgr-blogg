jQuery(document).ready(function($){
    
    var refreshButton = $('<button/>', {
        text: 'Save widgets',
        id: 'vgrblogg_force_reload',
        click: function (e) {
            e.stopPropagation();
            window.location.reload();
        }
    });
    refreshButton.css({
        padding: '15px',
        background: '#53c361',
        color: 'white',
        fontSize: 16,
        cursor: 'pointer',
    });
    $('.widget-liquid-right').prepend(refreshButton);


    function vgrblogg_reset_footer_transient() {
    
        // Run our AJAX call to delete our site transient
        $.ajax({
            type : 'post',
            dataType : 'json',
            url : ajaxurl,
            data : {
                'action' : 'vgrblogg-reset-transient',
                'vgrblogg-widget-nonce' : vgrblogg_AJAX.vgrblogg_widget_nonce
            },
            error: function ( xhr, ajaxOptions, thrownError ) {
                console.log( thrownError );
                console.log( ajaxOptions );
            }
        });
    }
 
    // If one of our update buttons is clicked on a single widget
    $( '.widgets-holder-wrap' ).on( 'click', '.widget-control-remove, .widget-control-close, .widget-control-save', function() {
        // Get our parent, or sidebar, ID
        var widget_parent_div = $(this).parents().eq(5).attr( 'id' );
 
        // And if our parent div ID, or our sidebar ID, is one of the following
        if ( widget_parent_div == 'sidebar-2' || widget_parent_div == 'sidebar-3' ) {
            // Run our function
            vgrblogg_reset_footer_transient();
        }
    });
});