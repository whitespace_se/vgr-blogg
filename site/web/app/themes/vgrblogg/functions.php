<?php
function vgrblogg_cache_bust ($name) {
    $revManifestPath = get_stylesheet_directory(). '/assets/dist/rev-manifest.json';
    if (file_exists($revManifestPath)) {
        $revManifest = json_decode(file_get_contents($revManifestPath), true);
    }
    return isset($revManifest) && !empty($revManifest[$name])
        ? $revManifest[$name]
        : $name;
}

include_once(get_stylesheet_directory().'/inc/acf-fields/featured-blog-posts.php');
if (is_multisite() && is_main_site()) {
    include_once(get_stylesheet_directory().'/inc/acf-fields/cta-create-a-blog.php');
}

include_once(get_stylesheet_directory().'/inc/plugins/acf/multisite-post-selector.php');

include_once(get_stylesheet_directory().'/inc/template-tags.php');

include_once(get_stylesheet_directory().'/inc/headers/security.php');

include_once(get_stylesheet_directory().'/inc/theme/enqueue.php');
include_once(get_stylesheet_directory().'/inc/theme/sidebars.php');
include_once(get_stylesheet_directory().'/inc/theme/icons.php');

include_once(get_stylesheet_directory().'/inc/theme/templates/blogs.php');
include_once(get_stylesheet_directory().'/inc/theme/templates/multisite-main-front.php');
