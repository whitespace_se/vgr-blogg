var plan = require('flightplan');

let argv = process.argv;

var config = {
	projectName: 'vgrfokus',
	keepReleases: 5,
	dumpFolder: '~/dumps/'
};

// plan.target('dev', {
// 	host: 'whitespace.work',
// 	username: 'whitespace',
// 	agent: process.env.SSH_AUTH_SOCK
// }, {
// 	host: 'whitespace.work',
// 	username: 'whitespace',
// 	root:'/srv/www',
// 	targets: [ 'vgrfokus.whitespace.work' ],
// 	user: 'web'
// });

plan.target('stage', {
    host: '91.201.60.14',
    username: 'nyheterv',
    privateKey: __dirname + '/ssh/ws_id_rsa',
    agent: process.env.SSH_AUTH_SOCK
}, {
    host: '91.201.60.14',
    username: 'nyheterv',
    root:'/home/nyheterv/whitespace-deploy',
    targets: [ 'vgrblogg.stagespace.se' ],
    user: 'nyheterv'
});

// plan.target('prod', {
//     host: '91.201.60.14',
//     username: 'nyheterv',
//     privateKey: __dirname + '/ssh/ws_id_rsa',
//     agent: process.env.SSH_AUTH_SOCK
// }, {
//     host: '91.201.60.14',
//     username: 'nyheterv',
//     root:'/home/nyheterv/whitespace-deploy',
//     targets: [ 'vgrfokus.se' ],
//     user: 'nyheterv'
// });

var shared = {
	'.env': true,
	'uploads': 'web/app/uploads'
};

/*
* init
*
* Initialize the server by setting up a suitable project structure.
*/
plan.remote('init', function(remote) {
	var root = plan.runtime.options.root;
	var targets = plan.runtime.options.targets;
	var user = plan.runtime.options.user;

	targets.forEach(function(target) {
    	var dir = root + '/' + target;
        var createReleasesCommand = 'mkdir -p ' + dir + '/releases';
        var createSharedCommand = 'mkdir -p ' + dir + '/shared';

        if (plan.runtime.target == 'prod' || plan.runtime.target == 'stage') { // Prod nor stage does not support sudo
            remote.exec(createReleasesCommand);
            remote.exec(createSharedCommand);
        } else {
        	remote.sudo(createReleasesCommand, { user: user });
        	remote.sudo(createSharedCommand, { user: user });
        }
	});
});

/*
 * deploy
 *
 * Deploy a release to the server. Will deploy to a specific sub folder
 * and symlink it as the current release.
 */
var time = new Date().toISOString();
var tmpDir = '/tmp/app' + time;
let filesFromDocker = argv.indexOf('--files-from=docker') !== -1;
let deployFrom = filesFromDocker ? 'tmp-docker-wp-folder' : '.';

plan.local('deploy', function(local) {
    if (filesFromDocker) {
       local.exec('./flightplan-docker.sh');
    }
    local.with('cd ' + deployFrom, function() {
    	local.log('Building the site');
    	local.exec('composer dump-autoload --optimize');
    	local.log('Copying release');

    	var ignoredFilesAndFolders = [
    		'grep -v node_modules',
    		'grep -v "web/app/uploads"',
    		//'grep -v spacecraft',
    		//'grep -v "templates/components"'
    	];
    	var grep = ignoredFilesAndFolders.toString().replace(/,/g, ' | ');

    	var files = local.find('config vendor web -type f | ' + grep, { silent: true });
    	// var components = local.find('web/app/themes/*/templates/components -name "*.twig"', { silent: true });

    	//files.stdout += components.stdout;

    	local.transfer(files, tmpDir);
    });
});

plan.remote('deploy', function(remote) {
	var root = plan.runtime.options.root;
	var targets = plan.runtime.options.targets;
	var user = plan.runtime.options.user;

	targets.forEach(function(target) {
	var dir = root + '/' + target;
	var release = dir + '/releases/' + time;

	remote.log('Deploying subsite ' + target + ' to ' + release);


    var copyTmpDirCommand = 'cp -R ' + tmpDir + ' ' + release;
    if (plan.runtime.target == 'prod' || plan.runtime.target == 'stage') { // Prod nor stage does not support sudo
        remote.exec(copyTmpDirCommand);
    } else {
        remote.sudo(copyTmpDirCommand, { user: user });
    }

	Object.keys(shared).forEach(function(key) {
		var s = dir + '/shared/' + key;
		var name = shared[key];
		if(name === true) {
		name = key;
		}
		var t = release + '/' + name;
        var linkSharedFilesAndFoldersCommand = 'ln -s ' + s + ' ' + t;
        if (plan.runtime.target == 'prod' || plan.runtime.target == 'stage') { // Prod nor stage does not support sudo
            remote.exec(linkSharedFilesAndFoldersCommand);
        } else {
            remote.sudo(linkSharedFilesAndFoldersCommand, { user: user });
        }
	});

    var removeCurrentFolderCommand = 'rm ' + dir + '/current';
    var linkCurrentReleaseCommand = 'ln -s ' + dir + '/releases/' + time + ' ' + dir + '/current';
    if (plan.runtime.target == 'prod' || plan.runtime.target == 'stage') { // Prod nor stage does not support sudo
        remote.exec(removeCurrentFolderCommand, { failsafe: true });
        remote.exec(linkCurrentReleaseCommand);
    } else {
        remote.sudo(removeCurrentFolderCommand, { failsafe: true, user: user });
        remote.sudo(linkCurrentReleaseCommand, { user: user });
    }

	remote.log('Checking for stale releases');
	var releases = getReleases(remote,dir);
	if (releases.length > config.keepReleases) {
		var removeCount = releases.length - config.keepReleases;
		remote.log('Removing ' + removeCount + ' stale release(s)');
		releases = releases.slice(0, removeCount);
		releases = releases.map(function (item) {
		return dir + '/releases/' + item;
		});
        var removeStaleReleaseCommand = 'rm -rf ' + releases.join(' ');
        if (plan.runtime.target == 'prod' || plan.runtime.target == 'stage') { // Prod nor stage does not support sudo
            remote.exec(removeStaleReleaseCommand);
        } else {
            remote.sudo(removeStaleReleaseCommand, { user: user });
        }
	}
	});

	remote.rm('-R ' + tmpDir);
});

plan.remote('rollback', function(remote) {
	//usage fly rollback:staging
	var root = plan.runtime.options.root;
	var targets = plan.runtime.options.targets;
	var user = plan.runtime.options.user;

	targets.forEach(function(target) {
	var dir = root + '/' + target;
	remote.log('Rolling back release');
	var releases = getReleases(remote,dir);
	if (releases.length > 1) {
		var oldCurrent = releases.pop();
		var newCurrent = releases.pop();
        var linkCurrentCommand = 'ln -nfs ' + dir + '/releases/' + newCurrent + ' '+ dir + '/current';
        var removePreviousCommand = 'rm -rf ' + dir + '/releases/' + oldCurrent;

        if (plan.runtime.target == 'prod' || plan.runtime.target == 'stage') { // Prod nor stage does not support sudo
            remote.log('Linking current to ' + newCurrent);
            remote.exec(linkCurrentCommand);
            remote.log('Removing ' + oldCurrent);
            remote.exec(removePreviousCommand);
        } else {
            remote.log('Linking current to ' + newCurrent);
            remote.sudo(linkCurrentCommand, { user: user });
            remote.log('Removing ' + oldCurrent);
            remote.sudo(removePreviousCommand, { user: user });
        }
	}

	});

});


plan.remote('fetch-db', function(remote) {
	let localConfig = require('./config',  {failsafe: false});
	let remoteConfig = require('./config.' + plan.runtime.target,  {failsafe: false});
	remote.log('Creating a remote database backup');
	
    var mysqlDumpCommand = 'mysqldump --replace -u' + remoteConfig.user + ' -p' + remoteConfig.password + ' ' + remoteConfig.database + ' > /tmp/' + localConfig.database + '.sql';
    if (plan.runtime.target == 'prod' || plan.runtime.target == 'stage') { // Prod nor stage does not support sudo
        remote.exec(mysqlDumpCommand);
    } else {
        remote.sudo(mysqlDumpCommand, { user: user });
    }
});

plan.local('fetch-db', function(local) {
	let localConfig = require('./config',  {failsafe: false});
	let remoteConfig = require('./config.' + plan.runtime.target,  {failsafe: false});

	let host = plan.runtime.options.host;
	let user = plan.runtime.options.username;
	let database = plan.runtime.options.database;

	local.log('Rsyncing down database backup');
	local.exec('rsync -qaHxv --numeric-ids --delete --progress -e "ssh -T -o Compression=no -x" '+ user +'@' + host + ':/tmp/' + localConfig.database + '.sql ' + config.dumpFolder);

	local.log('Creating a local database backup');
	if(localConfig.password !== '') {
		local.exec('mysqldump -u' + localConfig.user + ' -p' + localConfig.password + ' ' + localConfig.database + '| bzip2 -c > ' + config.dumpFolder + config.projectName + '_$(date +%Y-%m-%d-%H.%M.%S).sql.bz2');
		local.log('Truncating tables');
		local.exec('mysql -u' + localConfig.user + ' -p' + localConfig.password + ' -Nse \'show tables\' ' + localConfig.database + ' | while read table; do mysql -u' + localConfig.user + ' -p' + localConfig.password + ' -e "truncate table $table" ' + localConfig.database +'; done');
		local.log('Restoring database');
		local.exec('mysql -u' + localConfig.user + ' -p' + localConfig.password + ' ' + localConfig.database + ' < ' + config.dumpFolder + localConfig.database + '.sql');
	} else {
		local.exec('mysqldump -u' + localConfig.user + ' ' + localConfig.database + '| bzip2 -c > ' + config.dumpFolder + config.projectName + '_$(date +%Y-%m-%d-%H.%M.%S).sql.bz2');
		local.log('Truncating tables');
		local.exec('mysql -u' + localConfig.user + ' -Nse \'show tables\' ' + localConfig.database + ' | while read table; do mysql -u' + localConfig.user + ' -e "truncate table $table" ' + localConfig.database +'; done');
		local.log('Restoring database');
		local.exec('mysql -u' + localConfig.user + ' ' + localConfig.database + ' < ' + config.dumpFolder + localConfig.database + '.sql');
	}
	local.log('Finished');
});

plan.local('fetch-db', function(local) {
	
	let host = plan.runtime.options.host;
	let username = plan.runtime.options.username;

	//sync acf folder
	// local.log('syncing acf folder');
	// local.exec('rsync -qaHxv --numeric-ids --delete --progress -e "ssh -T -o Compression=no -x" '+ username +'@' + host + ':/srv/www/sgbc.whitespace.work/current/web/app/themes/sgbc/acf-json/ web/app/themes/sgbc/acf-json/');
	
	//sync uploads folder
	local.log('syncing uploads folder');
	local.exec('rsync -qaHxv --numeric-ids --delete --progress -e "ssh -T -o Compression=no -x" '+ username +'@' + host + ':/srv/www/sgbc.whitespace.work/current/web/app/uploads/ web/app/uploads/');
	
});

/*
 * Push database
 */
plan.local('push-db', function(local) {
  let localConfig = require('./config.local',  {failsafe: false});
  let remoteConfig = require('./config.' + plan.runtime.target,  {failsafe: false});
  let localSyncPath = config.dumpFolder + config.projectName + '-sync.sql';

	local.log('Creating a local database backup');

  if(localConfig.password !== '') {
		local.exec('mysqldump -u' + localConfig.user + ' -p' + localConfig.password + ' ' + localConfig.database + ' > ' + localSyncPath);
	} else {
		local.exec('mysqldump -u' + localConfig.user + ' ' + localConfig.database + ' > ' + localSyncPath);
	}

  local.log('Rsyncing up database backup');

  let host = plan.runtime.options.host;
	let user = plan.runtime.options.username;
	let database = localConfig.database;

  local.exec('rsync -qaHxv --numeric-ids --delete --progress -e "ssh -T -o Compression=no -x" '+ localSyncPath + ' ' + user +'@' + host + ':/tmp/' + database + '.sql');
});

plan.remote('push-db', function(remote) {
  let localConfig = require('./config.local',  {failsafe: false});
  let remoteConfig = require('./config.' + plan.runtime.target,  {failsafe: false});

  remote.exec('mysqldump -u' + remoteConfig.user + ' -p' + remoteConfig.password + ' ' + remoteConfig.database + '| bzip2 -c >  /tmp/' + config.projectName + '_' + time + '.sql.bz2');
  remote.log('Truncating tables');
  remote.exec('mysql -u' + remoteConfig.user + ' -p' + remoteConfig.password + ' -Nse \'show tables\' ' + remoteConfig.database + ' | while read table; do mysql -u' + remoteConfig.user + ' -p' + remoteConfig.password + ' -e "truncate table $table" ' + remoteConfig.database +'; done');
  remote.log('Importing database');
  remote.exec('mysql -u' + remoteConfig.user + ' -p' + remoteConfig.password + ' ' + remoteConfig.database + ' < /tmp/' + config.projectName + '.sql');
});

	
function getReleases(remote,dir) {
  var releases = remote.exec('ls ' + dir +
	'/releases', {silent: true});

  if (releases.code === 0) {
	releases = releases.stdout.trim().split('\n');
	return releases;
  }

  return [];
}
